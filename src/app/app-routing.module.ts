import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from './components/home/home.component';
import { ContactUsComponent} from './components/contact-us/contact-us.component';
import { FeatureComponent } from './components/feature/feature.component';


const routes: Routes = [
  {path: '' , component: HomeComponent},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'feature', component: FeatureComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
