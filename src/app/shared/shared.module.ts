import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent, FooterComponent, PreloaderComponent } from './components';



const COMPONENTS= [HeaderComponent, FooterComponent, PreloaderComponent]
@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule
  ],
  providers: [],
  exports: [...COMPONENTS]
})
export class SharedModule { }
